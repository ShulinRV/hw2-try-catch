const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const root = document.querySelector("#root");
const fullInfo = ["author", "name", "price"];

function correctList(item) {
  for (let value of fullInfo) {
    try {
      if (!item[value]) {
        throw new Error(`${value} not found`);
      }
    } catch (err) {
      console.log(err.message);
      return false;
    }
  }
  return true;
}

function setCorrectList(items) {
  const ulElement = document.createElement("ul");
  ulElement.classList.add("new__item");
  root.append(ulElement);

  items.forEach((item) => {
    const liElems = document.createElement("li");
    liElems.classList.add("items");

    for (let value of fullInfo) {
      const bookValue = document.createElement("div");
      bookValue.classList.add(value);
      bookValue.innerText = `${value} : ${item[value]}`;

      liElems.append(bookValue);
      ulElement.append(liElems);
    }
  });
}

function getCorrectBooks(books) {
  const result = [];

  books.forEach((item) => {
    if (correctList(item)) {
      result.push(item);
    }
  });
  setCorrectList(result);
}
getCorrectBooks(books);
